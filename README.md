# Basit ASP .NET Core MVC Uygulaması

### Test amaçlı oluşturulan bu uygulama basitçe bir not giriş simülasyonudur.

* .NET 6.0 kullanılmıştır.

* Ubuntu 22.04 (jammy) orijinal mirrorlardan çekilen paketler: 
    - `sudo apt install dotnet-runtime-6.0 aspnetcore-runtime-6.0 dotnet-sdk-6.0`

* Host üzerindeki PACKAGE,SDK,RUNTIME,VERSION bilgileri:
    - ```
        bugra@demir:~$ apt list --installed | grep -i "dotnet"
        dotnet-apphost-pack-6.0/jammy-updates,jammy-security,now 6.0.121-0ubuntu1~22.04.1 amd64 [kurulu,otomatik]
        dotnet-host/jammy-updates,jammy-security,now 6.0.121-0ubuntu1~22.04.1 amd64 [kurulu,otomatik]
        dotnet-hostfxr-6.0/jammy-updates,jammy-security,now 6.0.121-0ubuntu1~22.04.1 amd64 [kurulu,otomatik]
        dotnet-runtime-6.0/jammy-updates,jammy-security,now 6.0.121-0ubuntu1~22.04.1 amd64 [kurulu]
        dotnet-sdk-6.0/jammy-updates,jammy-security,now 6.0.121-0ubuntu1~22.04.1 amd64 [kurulu]
        dotnet-targeting-pack-6.0/jammy-updates,jammy-security,now 6.0.121-0ubuntu1~22.04.1 amd64 [kurulu,otomatik]
        dotnet-templates-6.0/jammy-updates,jammy-security,now 6.0.121-0ubuntu1~22.04.1 amd64 [kurulu,otomatik]
        ```
    - ```
        bugra@demir:~$ dotnet --version
        6.0.121

        bugra@demir:~$ dotnet --list-runtimes
        Microsoft.AspNetCore.App 6.0.21 [/usr/lib/dotnet/   shared/Microsoft.AspNetCore.App]
        Microsoft.NETCore.App 6.0.21 [/usr/lib/dotnet/shared/Microsoft.NETCore.App]

        bugra@demir:~$ dotnet --list-sdks
        6.0.121 [/usr/lib/dotnet/sdk]
        ```

* Uygulamanın build/run edilmesi ve test amaçlı gösterim:
    - gif dosyası için bkz.
    ![](app-build-and-show.gif)
