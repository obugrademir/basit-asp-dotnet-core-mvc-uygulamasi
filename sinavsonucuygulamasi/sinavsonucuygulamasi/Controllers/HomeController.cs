﻿using Microsoft.AspNetCore.Mvc;
using sinavsonucuygulamasi.Models;
using System.Diagnostics;

namespace sinavsonucuygulamasi.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ViewResult SinavSonucFormu()
        {
            return View();
        }

        [HttpPost]
        public ViewResult SinavSonucFormu(Sonuc OgrencininBasariDurumu)
        {
            if (ModelState.IsValid)
            {
                Repository.SonucEkle(OgrencininBasariDurumu);
                return View("Tesekkurler", OgrencininBasariDurumu);
            }
            else
            {
                return View();
            }
        }

        public ViewResult BasariliOgrencileriListele()
        {
            return View(Repository.Sonuclar.Where(x => x.SinavSonucu == true));
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}