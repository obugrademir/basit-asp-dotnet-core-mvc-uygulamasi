﻿using System.ComponentModel.DataAnnotations;

namespace sinavsonucuygulamasi.Models
{
    public class Sonuc
    {
        [Required(ErrorMessage = "Bu alanda Ad bilgisi boş bırakılamaz!")]
        public string Ad { get; set; }

        [Required(ErrorMessage = "Bu alanda Soyad bilgisi boş bırakılamaz!")]
        public string Soyad { get; set; }

        [Required(ErrorMessage = "Bu alanda Ogrenci Numarası bilgisi boş bırakılamaz!")]
        public string OgrenciNo { get; set; }

        [Required(ErrorMessage = "Lütfen başarı durumu ile ilgili bir seçim yapınız!")]
        public bool? SinavSonucu { get; set; }
    }
}
