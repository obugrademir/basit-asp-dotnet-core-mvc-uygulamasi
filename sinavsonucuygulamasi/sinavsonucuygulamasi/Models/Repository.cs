﻿namespace sinavsonucuygulamasi.Models
{
    public class Repository
    {
        private static List<Sonuc> sonuclar = new List<Sonuc> { };
        public static IEnumerable<Sonuc> Sonuclar => sonuclar;
        public static void SonucEkle(Sonuc sonuc)
        {
            sonuclar.Add(sonuc);
        }
    }
}
